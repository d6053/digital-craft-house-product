package main

import (
	"context"
	config "digital-craft-house-product/src/configuration"
	"digital-craft-house-product/src/http/rest"

	"digital-craft-house-product/src/storage/mysql"

	"log"
)

func main() {
	ctx := context.Background()

	config, err := config.LoadEnv()
	if err != nil {
		log.Fatalf(err.Error())
	}

	//Establish DB connection
	db, err := mysql.InitializeMySqlDB(config.MySQLDB.Username, config.MySQLDB.Password, config.MySQLDB.Host, config.MySQLDB.Name)
	if err != nil {
		log.Fatalf(err.Error())
	}

	server := rest.NewServer(config.Http.Port)
	server.Init(ctx, db)

	server.Run(config.Http.AllowedOrigins)
}

package query

const ListProducts = "SELECT * FROM `product`"

const GetProductsByUuid = "SELECT * FROM `product` WHERE uuid=?"

const CreateProduct = "INSERT INTO `product` (uuid, title, description,categories,price,time_published) VALUES (?, ?, ?, ?, ?, ?);"

const UpdateProduct = "UPDATE `product` SET title=?, description=?, categories=?, price=? WHERE uuid=?;"

const DeleteProduct = "DELETE FROM `product` WHERE uuid = ?"

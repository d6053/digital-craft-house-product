package migration

const CreateProductTable = "CREATE TABLE IF NOT EXISTS product (uuid VARCHAR(255) PRIMARY KEY, title VARCHAR(255) NOT NULL, description VARCHAR(255), categories  VARCHAR(255), price INTEGER, time_published TIMESTAMP);"

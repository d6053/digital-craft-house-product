package rest

import (
	"context"
	"digital-craft-house-product/src/domain/product"
	mock_product "digital-craft-house-product/src/domain/product/mock"
	"fmt"

	"encoding/json"
	"io/ioutil"
	"strings"
	"time"

	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-playground/validator"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"github.com/matryer/is"
)

const port = "9000"

var fixedTime = func() time.Time {
	t, err := time.Parse("2006-01-02 15:04:05", "2017-01-20 01:02:03")
	if err != nil {
		log.Fatalf("Error occured while parsing time %s", err)
	}

	return t
}

var p = product.Product{
	UUID:          "8ed4592e-ef42-47d7-bb0f-c69a4bf2c81f",
	Title:         "Lamp",
	Description:   "Lorem ipsum",
	Categories:    []string{"Wood", "Carving"},
	Price:         3333,
	TimePublished: fixedTime(),
}

var collection = product.ProductCollection{
	{
		UUID:          "8ed4592e-ef42-47d7-bb0f-c69a4bf2c81f",
		Title:         "Lamp",
		Description:   "Lorem ipsum",
		Categories:    []string{"Wood", "Carving"},
		Price:         3333,
		TimePublished: fixedTime(),
	}, {
		UUID:          "831242e-ef42-sds23-bb0f-c69a4asdas21",
		Title:         "Chair",
		Description:   "Lorem ipsum",
		Categories:    []string{"Wood"},
		Price:         2222,
		TimePublished: fixedTime(),
	},
}

func TestCreateProduct(t *testing.T) {
	is := is.New(t)

	controller := gomock.NewController(t)
	defer controller.Finish()

	mockService := mock_product.NewMockProductService(controller)

	server := NewServer(port)
	server.ProductService = mockService
	server.Validator = validator.New()

	ctx := context.Background()

	params := &product.CreateProductParams{
		UUID:          p.UUID,
		Title:         p.Title,
		Description:   p.Description,
		Categories:    p.Categories,
		Price:         p.Price,
		TimePublished: p.TimePublished,
	}

	mockService.EXPECT().CreateProduct(ctx, params).Return(&p, nil)

	bytes, err := json.Marshal(params)
	if err != nil {
		log.Fatalf("an error '%s' was not expected when marshalling to bytes", err)
	}

	url := fmt.Sprintf("/api/v1/products")

	request := httptest.NewRequest(http.MethodPost, url, strings.NewReader(string(bytes)))
	request.Header.Set("Content-Type", "application/json")

	recorder := httptest.NewRecorder()

	handler := http.HandlerFunc(server.CreateProduct)
	handler.ServeHTTP(recorder, request)

	r := recorder.Result()
	defer r.Body.Close()

	if status := recorder.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatalf("an error '%s' was not expected when reading the response body", err)
	}

	var response product.Product
	if err := json.Unmarshal(body, &response); err != nil {
		log.Fatalf("an error '%s' was not expected when unmarschalling the response body", err)
	}

	is.NoErr(err)
	is.Equal(p.UUID, response.UUID)
	is.Equal(p.Title, response.Title)
	is.Equal(p.Description, response.Description)
	is.Equal(p.Categories, response.Categories)
	is.Equal(p.Price, response.Price)
	is.Equal(p.TimePublished, response.TimePublished)
}

func TestUpdateProduct(t *testing.T) {
	is := is.New(t)

	controller := gomock.NewController(t)
	defer controller.Finish()

	mockService := mock_product.NewMockProductService(controller)

	server := NewServer(port)
	server.ProductService = mockService
	server.Validator = validator.New()

	params := &product.UpdateProductParams{
		Title:       p.Title,
		Description: p.Description,
		Categories:  p.Categories,
		Price:       p.Price,
	}

	mockService.EXPECT().UpdateProduct(gomock.Any(), params, p.UUID).Return(&p, nil)

	bytes, err := json.Marshal(params)
	if err != nil {
		log.Fatalf("an error '%s' was not expected when marshalling to bytes", err)
	}

	url := fmt.Sprintf("/api/v1/products/%s", p.UUID)
	vars := map[string]string{
		"uuid": fmt.Sprint(p.UUID),
	}

	request := httptest.NewRequest(http.MethodPut, url, strings.NewReader(string(bytes)))
	request = mux.SetURLVars(request, vars)
	request.Header.Set("Content-Type", "application/json")

	recorder := httptest.NewRecorder()

	handler := http.HandlerFunc(server.UpdateProduct)
	handler.ServeHTTP(recorder, request)

	r := recorder.Result()
	defer r.Body.Close()

	if status := recorder.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatalf("an error '%s' was not expected when reading the response body", err)
	}

	var response product.Product
	if err := json.Unmarshal(body, &response); err != nil {
		log.Fatalf("an error '%s' was not expected when unmarschalling the response body", err)
	}

	is.NoErr(err)
	is.Equal(p.UUID, response.UUID)
	is.Equal(p.Title, response.Title)
	is.Equal(p.Description, response.Description)
	is.Equal(p.Categories, response.Categories)
	is.Equal(p.Price, response.Price)
	is.Equal(p.TimePublished, response.TimePublished)
}

func TestGetProductsByUuid(t *testing.T) {
	is := is.New(t)

	controller := gomock.NewController(t)
	defer controller.Finish()

	mockService := mock_product.NewMockProductService(controller)

	server := NewServer(port)
	server.ProductService = mockService
	server.Validator = validator.New()

	mockService.EXPECT().GetProductsByUuid(gomock.Any(), p.UUID).Return(&p, nil)

	url := fmt.Sprintf("/api/v1/products/%s", p.UUID)
	vars := map[string]string{
		"uuid": fmt.Sprint(p.UUID),
	}

	request := httptest.NewRequest(http.MethodGet, url, nil)
	request = mux.SetURLVars(request, vars)
	request.Header.Set("Content-Type", "application/json")

	recorder := httptest.NewRecorder()

	handler := http.HandlerFunc(server.GetProductsByUuid)
	handler.ServeHTTP(recorder, request)

	r := recorder.Result()
	defer r.Body.Close()

	if status := recorder.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatalf("an error '%s' was not expected when reading the response body", err)
	}

	var response product.Product
	if err := json.Unmarshal(body, &response); err != nil {
		log.Fatalf("an error '%s' was not expected when unmarschalling the response body", err)
	}

	is.NoErr(err)
	is.Equal(p.UUID, response.UUID)
	is.Equal(p.Title, response.Title)
	is.Equal(p.Description, response.Description)
	is.Equal(p.Categories, response.Categories)
	is.Equal(p.Price, response.Price)
	is.Equal(p.TimePublished, response.TimePublished)
}

func TestListProducts(t *testing.T) {
	is := is.New(t)

	controller := gomock.NewController(t)
	defer controller.Finish()

	mockService := mock_product.NewMockProductService(controller)

	server := NewServer(port)
	server.ProductService = mockService
	server.Validator = validator.New()

	mockService.EXPECT().ListProducts(gomock.Any()).Return(collection, nil)

	url := "/api/v1/products"

	request := httptest.NewRequest(http.MethodGet, url, nil)
	request.Header.Set("Content-Type", "application/json")

	recorder := httptest.NewRecorder()

	handler := http.HandlerFunc(server.ListProducts)
	handler.ServeHTTP(recorder, request)

	r := recorder.Result()
	defer r.Body.Close()

	if status := recorder.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatalf("an error '%s' was not expected when reading the response body", err)
	}

	var response product.ProductCollection
	if err := json.Unmarshal(body, &response); err != nil {
		log.Fatalf("an error '%s' was not expected when unmarschalling the response body", err)
	}

	is.NoErr(err)
	is.Equal(collection[0].UUID, response[0].UUID)
	is.Equal(collection[0].Title, response[0].Title)
	is.Equal(collection[0].Description, response[0].Description)
	is.Equal(collection[1].Categories, response[1].Categories)
	is.Equal(collection[1].Price, response[1].Price)
	is.Equal(collection[1].TimePublished, response[1].TimePublished)
}

func TestDeleteProduct(t *testing.T) {
	is := is.New(t)

	controller := gomock.NewController(t)
	defer controller.Finish()

	mockService := mock_product.NewMockProductService(controller)

	server := NewServer(port)
	server.ProductService = mockService
	server.Validator = validator.New()

	mockService.EXPECT().DeleteProduct(gomock.Any(), p.UUID).Return(nil)

	url := fmt.Sprintf("/api/v1/products/%s", p.UUID)
	vars := map[string]string{
		"uuid": fmt.Sprint(p.UUID),
	}

	request := httptest.NewRequest(http.MethodDelete, url, nil)
	request = mux.SetURLVars(request, vars)
	request.Header.Set("Content-Type", "application/json")

	recorder := httptest.NewRecorder()

	handler := http.HandlerFunc(server.DeleteProduct)
	handler.ServeHTTP(recorder, request)

	r := recorder.Result()
	defer r.Body.Close()

	if status := recorder.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	response, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatalf("an error '%s' was not expected when reading the response body", err)
	}

	is.Equal(string(response), `"Product was deleted"`)
	is.NoErr(err)
}

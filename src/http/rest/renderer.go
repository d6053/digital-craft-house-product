package rest

import (
	"context"
	"encoding/json"
	"net/http"
)

type ErrorResponse struct {
	Status  int    `json:"status"`
	Code    string `json:"code"`
	Message string `json:"message"`
}

func renderResponse(w http.ResponseWriter, status int, payload interface{}) {
	w.Header().Set("Content-Type", "application/json")

	response, err := json.Marshal(payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(status)
	w.Write(response)
}

func renderErrorResponse(ctx context.Context, w http.ResponseWriter, status int, code string, message string) {
	errorResponse := ErrorResponse{
		Status:  status,
		Code:    code,
		Message: message,
	}

	renderResponse(w, status, errorResponse)
}

package rest

import (
	"context"
	"database/sql"
	"digital-craft-house-product/src/domain/product"
	"digital-craft-house-product/src/util"

	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

type server struct {
	Server    *http.Server
	Router    *mux.Router
	Validator *validator.Validate

	ProductService product.ProductService
}

func NewServer(port string) *server {
	address := ":" + port

	svr := &server{
		Server: &http.Server{
			Addr:         address,
			WriteTimeout: 15 * time.Second,
			ReadTimeout:  15 * time.Second,
		},
		Router: mux.NewRouter(),
	}

	svr.Router.NotFoundHandler = http.HandlerFunc(handleNotFound)

	svr.routes()

	return svr
}

func (s *server) Init(ctx context.Context, db *sql.DB) {
	s.Validator = validator.New()

	productRepository, err := product.NewProductRepository(ctx, db)
	if err != nil {
		fmt.Println("Product repo can not be initialized")
	}

	s.ProductService = product.NewProductService(productRepository)
}

func (s *server) Run(allowedOrigins string) {

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{http.MethodGet, http.MethodPost, http.MethodDelete, http.MethodPut},
		AllowedHeaders:   []string{"Content-Type", "Accept", "Authorization"},
		AllowCredentials: true,
	})

	s.Server.Handler = c.Handler(s.Router)

	if err := s.Server.ListenAndServe(); err != nil {
		log.Println(err)
	}

}

func handleNotFound(w http.ResponseWriter, r *http.Request) {
	err := util.NewErrorf(util.ErrorStatusNotFound, util.ErrorCodeNotFound, "Endpoint was not found.")
	renderErrorResponse(r.Context(), w, util.ErrorStatusNotFound, util.ErrorCodeNotFound, err.Error())
}

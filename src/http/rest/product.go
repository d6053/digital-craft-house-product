package rest

import (
	"digital-craft-house-product/src/domain/product"
	"digital-craft-house-product/src/util"
	"encoding/json"
	"net/http"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
)

func (s *server) CreateProduct(w http.ResponseWriter, r *http.Request) {
	var request product.CreateProductParams

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, err.Error())
		return
	}

	defer r.Body.Close()

	if err := s.Validator.Struct(request); err != nil {
		errors := err.(validator.ValidationErrors)
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, errors.Error())
		return
	}

	product, err := s.ProductService.CreateProduct(r.Context(), &request)
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusCreated, product)
}

func (s *server) UpdateProduct(w http.ResponseWriter, r *http.Request) {
	var request product.UpdateProductParams

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, err.Error())
		return
	}

	defer r.Body.Close()

	if err := s.Validator.Struct(request); err != nil {
		errors := err.(validator.ValidationErrors)
		renderErrorResponse(r.Context(), w, util.ErrorStatusInvalid, util.ErrorCodeInvalid, errors.Error())
		return
	}

	vars := mux.Vars(r)

	product, err := s.ProductService.UpdateProduct(r.Context(), &request, vars["uuid"])
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusOK, product)
}

func (s *server) GetProductsByUuid(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	product, err := s.ProductService.GetProductsByUuid(r.Context(), vars["uuid"])
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusOK, product)
}

func (s *server) ListProducts(w http.ResponseWriter, r *http.Request) {
	products, err := s.ProductService.ListProducts(r.Context())
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusOK, products)
}

func (s *server) DeleteProduct(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	err := s.ProductService.DeleteProduct(r.Context(), vars["uuid"])
	if err != nil {
		renderErrorResponse(r.Context(), w, util.ErrorStatusInternal, util.ErrorCodeInternal, err.Error())
		return
	}

	renderResponse(w, http.StatusOK, "Product was deleted")
}

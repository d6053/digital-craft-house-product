package rest

func (s *server) routes() {

	s.Router.Use(CorsMiddleware)
	products := s.Router.PathPrefix("/products").Subrouter()

	products.HandleFunc("", s.CreateProduct).Methods("POST")
	products.HandleFunc("/{uuid}", s.UpdateProduct).Methods("PUT")
	products.HandleFunc("", s.ListProducts).Methods("GET")
	products.HandleFunc("/{uuid}", s.GetProductsByUuid).Methods("GET")
	products.HandleFunc("/{uuid}", s.DeleteProduct).Methods("DELETE")
}

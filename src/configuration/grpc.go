package config

import "os"

type Grpc struct {
	Host    string
	Server  string
	Client  string
	AuthKey string
}

func LoadGrpcEnv() Grpc {
	grpc := Grpc{
		Host:    os.Getenv("GRPC_HOST"),
		Server:  os.Getenv("GRPC_SERVER"),
		Client:  os.Getenv("GRPC_CLIENT"),
		AuthKey: os.Getenv("AUTH_KEY"),
	}

	return grpc
}

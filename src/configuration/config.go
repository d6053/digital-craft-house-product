package config

import (
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	Application Application
	MySQLDB     MySQLDB
	Http        Http
	Grpc        Grpc
}

func LoadEnv() (*Config, error) {
	env := os.Getenv("APP_ENV")
	if env != "production" && env != "develop" {
		err := godotenv.Load()
		if err != nil {
			return nil, err
		}
	}

	application := LoadApplicationEnv()
	mySQLDB := LoadMySQLDBEnv()
	http := LoadHTTPEnv()
	grpc := LoadGrpcEnv()

	conf := Config{
		Application: application,
		MySQLDB:     mySQLDB,
		Http:        http,
		Grpc:        grpc,
	}

	return &conf, nil
}

package product

import "context"

//go:generate mockgen -source=service.go -destination=mock/mock_service.go -package=mock_product
type ProductService interface {
	ListProducts(ctx context.Context) (ProductCollection, error)
	CreateProduct(ctx context.Context, p *CreateProductParams) (*Product, error)
	UpdateProduct(ctx context.Context, p *UpdateProductParams, uuid string) (*Product, error)
	GetProductsByUuid(ctx context.Context, uuid string) (*Product, error)
	DeleteProduct(ctx context.Context, uuid string) error
}

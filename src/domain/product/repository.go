package product

import (
	"context"
)

//go:generate mockgen -source=repository.go -destination=mock/mock_repository.go -package=mock_product
type ProductRepository interface {
	ListProducts(ctx context.Context) (ProductCollection, error)
	GetProductsByUuid(ctx context.Context, uuid string) (Product, error)
	CreateProduct(ctx context.Context, arg CreateProductParams) (*Product, error)
	CreateProductSellerMapping(ctx context.Context, arg ProductSellerMapping) (*ProductSellerMapping, error)
	UpdateProduct(ctx context.Context, arg UpdateProductParams, uuid string) (*Product, error)
	DeleteProduct(ctx context.Context, uuid string) error
}

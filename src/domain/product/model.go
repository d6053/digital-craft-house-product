package product

import "time"

type Product struct {
	UUID          string    `json:"uuid"`
	Title         string    `json:"title"`
	Description   string    `json:"description"`
	Categories    []string  `json:"categories"`
	Price         int64     `json:"price"`
	TimePublished time.Time `json:"time_published"`
	SellerUUID    string    `json:"seller_uuid"`
}

type CreateProductParams struct {
	UUID          string    `json:"uuid"`
	Title         string    `json:"title"`
	Description   string    `json:"description"`
	Categories    []string  `json:"categories"`
	Price         int64     `json:"price"`
	TimePublished time.Time `json:"time_published"`
	SellerUUID    string    `json:"seller_uuid"`
}

type UpdateProductParams struct {
	Title       string   `json:"title"`
	Description string   `json:"description"`
	Categories  []string `json:"categories"`
	Price       int64    `json:"price"`
}

type ProductCollection []*Product

type ProductSellerMapping struct {
	ProductUUID string `json:"product_uuid"`
	SellerUUID  string `json:"seller_uuid"`
}

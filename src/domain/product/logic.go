package product

import (
	"context"
	"database/sql"
	"time"

	"github.com/google/uuid"
)

type productService struct {
	repository ProductRepository
}

func NewProductService(repository ProductRepository) ProductService {
	return &productService{
		repository: repository,
	}
}

// List all stored products
func (s *productService) ListProducts(ctx context.Context) (res ProductCollection, err error) {
	products, err := s.repository.ListProducts(ctx)
	if err != nil {
		return nil, err
	}

	return products, nil
}

// Show product by ID
func (s *productService) GetProductsByUuid(ctx context.Context, uuid string) (*Product, error) {
	product, err := s.repository.GetProductsByUuid(ctx, uuid)
	if err != nil {
		return nil, err
	}

	return &product, nil
}

// Add new product and return its uuid.
func (s *productService) CreateProduct(ctx context.Context, p *CreateProductParams) (*Product, error) {
	uuid := uuid.New().String()

	params := CreateProductParams{
		UUID:          uuid,
		Title:         p.Title,
		Description:   p.Description,
		Categories:    p.Categories,
		Price:         p.Price,
		TimePublished: time.Now(),
	}
	product, err := s.repository.CreateProduct(ctx, params)
	if err != nil {
		if err == sql.ErrNoRows {
			return product, nil
		}
		return nil, err
	}
	return product, nil
}

// Update existing product and return its uuid.
func (s *productService) UpdateProduct(ctx context.Context, p *UpdateProductParams, uuid string) (*Product, error) {

	params := UpdateProductParams{
		Title:       p.Title,
		Description: p.Description,
		Categories:  p.Categories,
		Price:       p.Price,
	}

	product, err := s.repository.UpdateProduct(ctx, params, uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			return product, nil
		}
		return nil, err
	}
	return product, nil
}

// Remove product from storage
func (s *productService) DeleteProduct(ctx context.Context, uuid string) error {
	err := s.repository.DeleteProduct(ctx, uuid)
	if err != nil {
		return err
	}
	return nil
}

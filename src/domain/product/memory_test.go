package product_test

// import (
// 	"context"
// 	"database/sql"
// 	"digital-craft-house-product/src/domain/product"
// 	"log"
// 	"regexp"
// 	"testing"

// 	"github.com/DATA-DOG/go-sqlmock"
// 	"github.com/matryer/is"
// )

// var pp = product.Product{
// 	UUID:          "8ed4592e-ef42-47d7-bb0f-c69a4bf2c81f",
// 	Title:         "Lamp",
// 	Description:   "Lorem ipsum",
// 	Categories:    []string{"Wood", "Carving"},
// 	Price:         3333,
// 	TimePublished: fixedTime(),
// }

// func NewMock() (*sql.DB, sqlmock.Sqlmock) {
// 	db, mock, err := sqlmock.New()
// 	if err != nil {
// 		log.Fatalf("an error '%s' was not expected when opening a stub database connection", err)
// 	}

// 	mock.ExpectExec(`CREATE TABLE IF NOT EXISTS product \(uuid VARCHAR\(255\) PRIMARY KEY, title VARCHAR\(255\) NOT NULL, description VARCHAR\(255\), categories VARCHAR\(255\), price INTEGER, time_published TIMESTAMP\);`).WillReturnResult(sqlmock.NewResult(1, 1))

// 	return db, mock
// }

// func TestRepositoryListProducts(t *testing.T) {

// 	is := is.New(t)

// 	ctx := context.Background()
// 	db, mock := NewMock()

// 	repo, err := product.NewProductRepository(ctx, db)
// 	if err != nil {
// 		log.Fatalf("an error occured '%s'", err)
// 	}

// 	rows := sqlmock.NewRows([]string{"uuid", "title", "description", "categories", "price", "time_published"})

// 	query := regexp.QuoteMeta("SELECT * FROM `product`")

// 	mock.ExpectQuery(query).WillReturnRows(rows)

// 	products, err := repo.ListProducts(ctx)

// 	is.NoErr(err)
// 	is.True(products != nil)
// }

// // func TestRepositoryGetProductsByUuid(t *testing.T) {

// // 	is := is.New(t)

// // 	ctx := context.Background()
// // 	db, mock := NewMock()

// // 	repo, err := product.NewProductRepository(ctx, db)
// // 	if err != nil {
// // 		log.Fatalf("an error occured '%s'", err)
// // 	}

// // 	rows := sqlmock.NewRows([]string{"uuid", "title", "description", "categories", "price", "time_published"})

// // 	query := regexp.QuoteMeta("SELECT * FROM `product` WHERE uuid=?")

// // 	mock.ExpectQuery(query).WillReturnRows(rows).WithArgs(pp.UUID)

// // 	product, err := repo.GetProductsByUuid(ctx, pp.UUID)

// // 	is.NoErr(err)
// // 	is.Equal(product.Price, p.Price)
// // }

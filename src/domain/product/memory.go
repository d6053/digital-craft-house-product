package product

import (
	"context"
	repository "digital-craft-house-product/src/storage/mysql"
	query "digital-craft-house-product/src/storage/mysql/query"

	"digital-craft-house-product/src/storage/mysql/migration"

	"database/sql"

	"github.com/lib/pq"
)

type productRepository struct {
	queries *repository.Queries
}

func NewProductRepository(ctx context.Context, db *sql.DB) (ProductRepository, error) {
	if _, err := db.ExecContext(ctx, migration.CreateProductTable); err != nil {
		return nil, err
	}
	if _, err := db.ExecContext(ctx, migration.CreateProductSellerTable); err != nil {
		return nil, err
	}
	return &productRepository{
		queries: repository.NewQueries(db),
	}, nil
}

func (repository *productRepository) ListProducts(ctx context.Context) (ProductCollection, error) {
	rows, err := repository.queries.DB.QueryContext(ctx, query.ListProducts)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	products := ProductCollection{}
	for rows.Next() {
		var p Product
		if err := rows.Scan(
			&p.UUID,
			&p.Title,
			&p.Description,
			pq.Array(&p.Categories),
			&p.Price,
			&p.TimePublished,
		); err != nil {
			return nil, err
		}
		products = append(products, &p)
	}
	if err := rows.Close(); err != nil {
		return nil, err
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return products, nil
}

func (repository *productRepository) GetProductsByUuid(ctx context.Context, uuid string) (Product, error) {
	row := repository.queries.DB.QueryRowContext(ctx, query.GetProductsByUuid, uuid)
	var p Product
	err := row.Scan(
		&p.UUID,
		&p.Title,
		&p.Description,
		pq.Array(&p.Categories),
		&p.Price,
		&p.TimePublished,
	)
	return p, err

}

func (repository *productRepository) CreateProduct(ctx context.Context, arg CreateProductParams) (*Product, error) {

	row := repository.queries.DB.QueryRowContext(ctx, query.CreateProduct,
		&arg.UUID,
		&arg.Title,
		&arg.Description,
		pq.Array(&arg.Categories),
		&arg.Price,
		&arg.TimePublished,
	)

	p := new(Product)
	err := row.Scan(
		&p.UUID,
		&p.Title,
		&p.Description,
		&p.Categories,
		&p.Price,
		&p.TimePublished,
	)

	return p, err
}

func (repository *productRepository) UpdateProduct(ctx context.Context, arg UpdateProductParams, uuid string) (*Product, error) {
	row := repository.queries.DB.QueryRowContext(ctx, query.UpdateProduct,
		&arg.Title,
		&arg.Description,
		pq.Array(&arg.Categories),
		&arg.Price,
		uuid,
	)

	p := new(Product)
	err := row.Scan(
		&p.Title,
		&p.Description,
		&p.Categories,
		&p.Price,
	)

	return p, err
}

func (repository *productRepository) DeleteProduct(ctx context.Context, uuid string) error {
	_, err := repository.queries.DB.ExecContext(ctx, query.DeleteProduct, uuid)
	return err
}

func (repository *productRepository) CreateProductSellerMapping(ctx context.Context, arg ProductSellerMapping) (*ProductSellerMapping, error) {
	row := repository.queries.DB.QueryRowContext(ctx, query.CreateProductSellerMapping,
		&arg.ProductUUID,
		&arg.SellerUUID,
	)

	m := new(ProductSellerMapping)
	err := row.Scan(
		&m.ProductUUID,
		&m.SellerUUID,
	)

	return m, err
}

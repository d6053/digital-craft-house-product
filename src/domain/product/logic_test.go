package product_test

import (
	"context"
	"log"
	"testing"
	"time"

	"digital-craft-house-product/src/domain/product"
	mock_product "digital-craft-house-product/src/domain/product/mock"

	"github.com/golang/mock/gomock"
	"github.com/matryer/is"
)

var fixedTime = func() time.Time {
	t, err := time.Parse("2006-01-02 15:04:05", "2017-01-20 01:02:03")
	if err != nil {
		log.Fatalf("Error occured while parsing time %s", err)
	}

	return t
}

var p = product.Product{
	UUID:          "8ed4592e-ef42-47d7-bb0f-c69a4bf2c81f",
	Title:         "Lamp",
	Description:   "Lorem ipsum",
	Categories:    []string{"Wood", "Carving"},
	Price:         3333,
	TimePublished: fixedTime(),
}

var collection = product.ProductCollection{
	{
		UUID:          "8ed4592e-ef42-47d7-bb0f-c69a4bf2c81f",
		Title:         "Lamp",
		Description:   "Lorem ipsum",
		Categories:    []string{"Wood", "Carving"},
		Price:         3333,
		TimePublished: fixedTime(),
	}, {
		UUID:          "831242e-ef42-sds23-bb0f-c69a4asdas21",
		Title:         "Chair",
		Description:   "Lorem ipsum",
		Categories:    []string{"Wood"},
		Price:         2222,
		TimePublished: fixedTime(),
	},
}

func TestListProducts(t *testing.T) {
	t.Run("success", func(t *testing.T) {

		is := is.New(t)
		ctx := context.Background()
		controller := gomock.NewController(t)

		mockRepository := mock_product.NewMockProductRepository(controller)
		mockRepository.EXPECT().ListProducts(ctx).Return(collection, nil)

		service := product.NewProductService(mockRepository)

		result, err := service.ListProducts(ctx)

		is.NoErr(err)
		is.Equal(result, collection)
	})
}

func TestGetProductsByUuid(t *testing.T) {
	t.Run("success", func(t *testing.T) {

		is := is.New(t)
		ctx := context.Background()
		controller := gomock.NewController(t)

		mockRepository := mock_product.NewMockProductRepository(controller)
		mockRepository.EXPECT().GetProductsByUuid(ctx, p.UUID).Return(p, nil)

		service := product.NewProductService(mockRepository)

		result, err := service.GetProductsByUuid(ctx, p.UUID)

		is.NoErr(err)
		is.Equal(result, &p)
	})
}

func TestCreateProduct(t *testing.T) {
	t.Run("success", func(t *testing.T) {

		is := is.New(t)
		ctx := context.Background()
		controller := gomock.NewController(t)

		mockRepository := mock_product.NewMockProductRepository(controller)

		var params = product.CreateProductParams{
			UUID:          p.UUID,
			Title:         p.Title,
			Description:   p.Description,
			Categories:    p.Categories,
			Price:         p.Price,
			TimePublished: p.TimePublished,
		}
		mockRepository.EXPECT().CreateProduct(ctx, gomock.Any()).Return(&p, nil)

		service := product.NewProductService(mockRepository)

		result, err := service.CreateProduct(ctx, &params)

		is.NoErr(err)
		is.Equal(result, &p)
	})
}

func TestUpdateProduct(t *testing.T) {
	t.Run("success", func(t *testing.T) {

		is := is.New(t)
		ctx := context.Background()
		controller := gomock.NewController(t)

		mockRepository := mock_product.NewMockProductRepository(controller)

		var params = product.UpdateProductParams{
			Title:       p.Title,
			Description: p.Description,
			Categories:  p.Categories,
			Price:       p.Price,
		}
		mockRepository.EXPECT().UpdateProduct(ctx, params, p.UUID).Return(&p, nil)

		service := product.NewProductService(mockRepository)

		result, err := service.UpdateProduct(ctx, &params, p.UUID)

		is.NoErr(err)
		is.Equal(result, &p)
	})
}

func TestDeleteProduct(t *testing.T) {
	t.Run("success", func(t *testing.T) {

		is := is.New(t)
		ctx := context.Background()
		controller := gomock.NewController(t)

		mockRepository := mock_product.NewMockProductRepository(controller)
		mockRepository.EXPECT().DeleteProduct(ctx, p.UUID).Return(nil)

		service := product.NewProductService(mockRepository)
		err := service.DeleteProduct(ctx, p.UUID)

		is.NoErr(err)
	})
}

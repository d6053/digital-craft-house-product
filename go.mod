module digital-craft-house-product

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang/mock v1.6.0
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/lib/pq v1.10.5
	github.com/matryer/is v1.4.0
	github.com/rs/cors v1.8.2
	goa.design/goa/v3 v3.7.2
)
